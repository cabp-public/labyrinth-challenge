# Labyrinth

Repository for a coding example to solve a Labyrinth

## The Grid
You can set the amount of "tiles" in the labyrinth by changing the X or Y parameters in the URL:

```http://labyrinth.local/?x=10&y=10```

## Start / Exit / Obstacles
Those "tiles" are calculated randomly. Start tile is marked in green, exit in red and obstacles with gray.

## How it works
The script selects the next tile by analyzing all directions (i.e. North, South, East, West) and determining the one closes to the exit.
