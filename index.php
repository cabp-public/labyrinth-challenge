<?php
require_once 'Labyrinth.php';

$author = 'Carlos Bucheli Padilla';
$title = $author;
$description = 'Senior Engineer specialized in software development, with 17+ years experience using leading-edge technologies, born in Quito - Ecuador. Now living permanently in Buenos Aires - Argentina, he has recently performed several successful tech-talks, including the 2018 Campus Party.';

$data = $_GET;
$tilesX = isset($data['x']) && is_numeric($data['x']) ? $data['x']: 6;
$tilesY = isset($data['y']) && is_numeric($data['y']) ? $data['y']: 5;
$maxObstacles = round(($tilesX + $tilesY) / 2);

$labyrinth = new Labyrinth($tilesX, $tilesY, $maxObstacles);
$results = $labyrinth->go();

$s  = 'background: #f4f4f4; display: inline-block; max-height: 40em; overflow: auto;';
$s .= ' border: 1px solid #ccc; border-radius: 3px; padding: 0.5em; width: 98%;';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#212121">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">

  <link href="http://carlos-bucheli.com/" rel="canonical">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/assets/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="/assets/labyrinth.css" type="text/css" rel="stylesheet" media="screen,projection">

  <title><?= $title ?></title>
</head>

<body>

<div class="row">
  <h6 class="col s12">
    Start: <em>(<?=implode(',', $results['startTile'])?>)</em> Exit: <em>(<?=implode(',', $results['exitTile'])?>)</em>
    <small>Read documentation <a href="" target="_blank">here</a></small>
  </h6>
</div>
<div class="row">
  <div class="col s12"></div>
  <table class="centered responsive-table">
    <tbody>
    <?php for ($row = $results['grid']['y']; $row >= 0; $row--) { ?>
      <tr>
        <td class="axis"><?=$row?></td>
        <?php for ($col = 0; $col <= $results['grid']['x']; $col++) { ?>
            <?php
            $style = '';
            $cell = "($col,$row)";
            $content = '&nbsp;';

            // Obstacles
            foreach ($results['obstacleTiles'] as $tile) {
                $tileStr = '(' . implode(',', $tile) . ')';

                if ($tileStr === $cell) {
                    $style = ' class="obstacle"';
                    $content = '<small>obstacle</small>';
                    break;
                }
            }

            // Steps
            foreach ($results['history'] as $index => $step) {
                $stepStr = '(' . implode(',', $step) . ')';

                if ($stepStr === $cell) {
                    $style = ' class="step"';
                    $content = "<small>Step #" . ($index + 1) . " $stepStr</small>";
                    break;
                }
            }

            // Start
            $startStr = '(' . $results['startTile']['x'] . ',' . $results['startTile']['y'] . ')';
            if ($startStr === $cell) {
                $style = ' class="start"';
                $content = "Start $startStr";
            }

            // End
            $endStr = '(' . $results['exitTile']['x'] . ',' . $results['exitTile']['y'] . ')';
            if ($endStr === $cell) {
                $style = ' class="end"';
                $content = "Exit $endStr";
            }
            ?>
          <td<?=$style?>><?php echo $content; ?></td>
        <?php } ?>
      </tr>
    <?php } ?>
    <tr>
      <td>&nbsp;</td>
        <?php for ($col = 0; $col <= $results['grid']['x']; $col++) { ?>
          <td class="axis"><?=$col?></td>
        <?php } ?>
    </tr>
    </tbody>
  </table>
</div>
</div>

</body>

</html>
