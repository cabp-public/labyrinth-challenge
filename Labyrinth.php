<?php

final class Labyrinth
{
    private $axesAvail;
    private $axesAvailKeys;
    private $baseAxis;
    private $baseAxisKey;
    private $counterAxis;
    private $counterAxisKey;

    private $exitTile;
    private $startTile;
    private $obstacleTiles;
    private $labyrinth;

    public function __construct(int $tilesX, int $tilesY, int $maxObstacles)
    {
        $this->axesAvail = [
            'x' => range(0, $tilesX),
            'y' => range(0, $tilesY),
        ];

        $this->axesAvailKeys = array_keys($this->axesAvail);

        $base = rand(0, 1);

        $this->baseAxisKey = $this->axesAvailKeys[$base];
        $this->counterAxisKey = $this->baseAxisKey === $this->axesAvailKeys[0] ? $this->axesAvailKeys[1] : $this->axesAvailKeys[0];
        $this->baseAxis = $this->axesAvail[$this->baseAxisKey];
        $this->counterAxis = $this->axesAvail[$this->counterAxisKey];

        $this->exitTile = $this->generateExitTile();
        $this->startTile = $this->generateStartTile();
        $this->obstacleTiles = $this->generateObstacleTiles($maxObstacles);

        $this->labyrinth = [
            'grid' => [
                'x' => $tilesX,
                'y' => $tilesY,
            ],
            'startTile' => $this->startTile,
            'exitTile' => $this->exitTile,
            'obstacleTiles' => $this->obstacleTiles,
            'history' => [],
        ];

        $s  = 'background: #f4f4f4; display: block; height: 5em; overflow: auto;';
        $s .= ' border: 1px solid #ccc; border-radius: 3px; padding: 0.5em; width: 5em;';
        $s .= ' position: fixed; top: 1em; right: 1em;';
    }

    private function generateExitTile(): array
    {
        $output = [
            'x' => $this->axesAvail['x'][count($this->axesAvail['x']) - 1],
            'y' => $this->axesAvail['y'][count($this->axesAvail['y']) - 1],
        ];

        try {
            $exitBaseAxis = rand($this->baseAxis[0], $this->baseAxis[count($this->baseAxis) - 1]);
            $counterAxisMax = $this->counterAxis[count($this->counterAxis) - 1];
            $counterAxisLimits = [$this->counterAxis[0], $counterAxisMax];
            $exitCounterAxis = $counterAxisLimits[rand(0, 1)];

            $output[$this->baseAxisKey] = $exitBaseAxis;
            $output[$this->counterAxisKey] = $exitCounterAxis;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function generateStartTile(): array
    {
        $output = [
            'x' => $this->axesAvail['x'][0],
            'y' => $this->axesAvail['y'][0],
        ];

        try {
            $baseValue = rand($this->baseAxis[0], $this->baseAxis[count($this->baseAxis) - 1]);
            $counterValue = $this->counterAxis[0];

            $output[$this->baseAxisKey] = $baseValue;
            $output[$this->counterAxisKey] = $counterValue;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function generateObstacleTiles(int $maxObstacles): array
    {
        $output = [];

        try {
            for ($i = 0; $i < $maxObstacles; $i++) {
                $x = rand($this->axesAvail['x'][0], $this->axesAvail['x'][count($this->axesAvail['x']) - 1]);
                $y = rand($this->axesAvail['y'][0], $this->axesAvail['y'][count($this->axesAvail['y']) - 1]);
                $obstacle = [
                    'x' => $x,
                    'y' => $y,
                ];

                if ($obstacle !== $this->exitTile) {
                    $output[] = [
                        'x' => $x,
                        'y' => $y,
                    ];
                }
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function isTileWithinLimits(array $tile): bool
    {
        $output = false;
        $valid = [
            'negative' => false,
            'beyond' => false,
        ];

        try {
            $xMax = $this->axesAvail['x'][count($this->axesAvail['x']) - 1];
            $yMax = $this->axesAvail['y'][count($this->axesAvail['y']) - 1];
            $valid['negative'] = $tile['x'] >= 0 && $tile['y'] >= 0;
            $valid['beyond'] = $tile['x'] <= $xMax && $tile['y'] <= $yMax;

            $output = $valid['negative'] && $valid['beyond'];
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function getPossibleDirections(array $tile): array
    {
        $output = [];

        try {
            $coordinatesRaw = [
                'n' => [ 'x' => $tile['x'], 'y' => $tile['y'] + 1],
                's' => [ 'x' => $tile['x'], 'y' => $tile['y'] - 1],
                'e' => [ 'x' => $tile['x'] + 1, 'y' => $tile['y']],
                'w' => [ 'x' => $tile['x'] - 1, 'y' => $tile['y']],
            ];

            foreach ($coordinatesRaw as $direction => $coordinate) {
                if ($this->isTileWithinLimits($coordinate)) {
                    $output[$direction] = $coordinate;
                }
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function checkForObstacles(array $coordinates): array
    {
        $output = [];

        try {
            foreach ($coordinates as $direction => $coordinate) {
                $hasObstacle = in_array($coordinate, $this->obstacleTiles);
                $stepTaken = in_array($coordinate, $this->labyrinth['history']);

                if (!$hasObstacle && !$stepTaken) {
                    $output[$direction] = $coordinate;
                }
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    private function findNextTile(array $tile): array
    {
        $s  = 'background:#f4f4f4; display:block; height:20em; overflow:auto; border:1px solid #ccc; border-radius: 3px;';
        $s .= ' padding: 0.5em; float: left; width: 30%; margin: 0.56em';

        $output = [];
        $coordinatesDiff = [];
        $coordinatesSorted = [];

        try {
            $coordinatesPossible = $this->getPossibleDirections($tile);

            // Difference
            foreach ($coordinatesPossible as $direction => $coordinate) {
                $diffX = $this->exitTile['x'] - $coordinate['x'];
                $diffY = $this->exitTile['y'] - $coordinate['y'];
                $coordinateDiff = [
                    'x' => $diffX < 0 ? ($diffX * -1) : $diffX,
                    'y' => $diffY < 0 ? ($diffY * -1) : $diffY,
                ];

                $coordinatesDiff[$direction] = $coordinateDiff;
            }

            // Order directions from more to least possible
            array_multisort($coordinatesDiff, SORT_ASC);

            foreach ($coordinatesDiff as $direction => $coordinateDiff) {
                $coordinatesSorted[$direction] = $coordinatesPossible[$direction];
            }

            // Check for obstacles
            $coordinatesClean = $this->checkForObstacles($coordinatesSorted);
            $coordinatesCleanKeys = array_keys($coordinatesClean);
            $output = $coordinatesClean[$coordinatesCleanKeys[0]];

        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }

    public function go()
    {
        $s  = 'background: #f4f4f4; display: inline-block; max-height: 40em; overflow: auto;';
        $s .= ' border: 1px solid #ccc; border-radius: 3px; padding: 0.5em; width: 98%;';

        $output = [];

        try {
            $currentTile = $this->startTile;

            $i = 0;

            do {
                $nexTile = $this->findNextTile($currentTile);
                $currentTile = $nexTile;
                $this->labyrinth['history'][] = $currentTile;

                if ($i >= 50) {
                    $currentTile = $this->exitTile;
                }

                $i++;
            } while ($currentTile !== $this->exitTile);

            $output = $this->labyrinth;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage();
        }
        finally {
            return $output;
        }
    }
}
